FROM ubuntu

RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y python3 python3-pip
COPY app /usr/src/app
WORKDIR /usr/src/app
RUN echo $(ls)
RUN echo $(pwd)
RUN pip3 install -r requirements.txt
CMD ["python3", "api.py"]
