## To Run:
1) Install Docker [instructions](https://docs.docker.com/v17.09/engine/installation/)
2) Install docker-compose [instructions](https://docs.docker.com/compose/install/)
3) Install python3 `sudo apt-get install python3`
4) Install python3-pip `sudo apt-get install python3-pip`
5) Install psycopg2 `pip3 install psycopg2-binary`
6) Build the chat api ```docker-compose build
docker-compose up -d
python3 app/scripts/build_dev_db.py```

7) There are some VERY simple tests (not what I'd use in a work environment... but these can take a long time time to develop. run them by `python3 app/scripts/very_basic_tests.py` Will just print some statements. Seeing any exception here should be an indication that something was installed improperly.
8) send requests to `http://localhost:5000/<channel>/` using postman or python requests.
