import yaml
from flask import Flask, request, abort
from controllers.chat_controller import ChatController, BadChannelIdException
import json

app = Flask(__name__)


with open("settings.yaml") as yaml_file:
    settings = yaml.safe_load(yaml_file)[
        "dev"
    ]  # todo: get environment from environment variable


@app.route("/<channel>/messages", methods=["POST"])
def add_message(channel):
    message_data = request.args
    try:
        return json.dumps(
            ChatController(settings).add_channel_message(channel, message_data)
        )
    except BadChannelIdException:
        return json.dumps({"error_message": "Bad channel name"}), 400


@app.route("/<channel>/messages", methods=["GET"])
def get_messages(channel):
    try:
        start_id = json.loads(request.args.get("start_id"))
    except TypeError:
        start_id = 0
    return json.dumps(
        ChatController(settings).get_messages_starting_at_message_id(channel, start_id)
    )


if __name__ == "__main__":
    app.run("0.0.0.0")
