import psycopg2

sql_command_list = [
    "create table if not exists chatapi_channel(id varchar(50))",
    "create table if not exists chatapi_user(id bigserial, username varchar(50))",
    "create table if not exists chatapi_channel_member(channel_id varchar(50), user_id bigint)",
    "create table if not exists chatapi_channel_message(id bigserial, channel_id varchar(50), user_id bigint, message varchar(2000))",
    "create table if not exists chatapi_direct_message(id bigserial, from_user_id bigint, to_user_id bigint, message varchar(2000))",
]


def create_db_if_not_exists():
    """trying to create database named chatapi... but cannot do that from transaction"""
    conn = psycopg2.connect(
        host="localhost", port=5432, user="postgres", password="postgres"
    )
    with conn.cursor() as curs:
        curs.execute("create database chatapi")
    conn.commit()
    conn.close()


def create_tables_if_not_exists():
    conn = psycopg2.connect(
        host="localhost", port=5432, user="postgres", password="postgres"
    )
    with conn.cursor() as curs:
        for sql in sql_command_list:
            curs.execute(sql)
    conn.commit()
    conn.close()


if __name__ == "__main__":
    create_tables_if_not_exists()
    print("Done!")
