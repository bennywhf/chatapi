import requests
import json


def create_message(message_text):
    return {"message": message_text, "username": "benny"}


def get_messages():
    response = requests.get(
        "http://localhost:5000/channel1/messages", params={"start_id": 0}
    )
    return response.content


def send_message(message):
    msg = create_message(message)
    response = requests.post("http://localhost:5000/channel1/messages", params=msg)
    print(response.content)


def bad_request():
    response = requests.post(
        "http://localhost:5000/channel__~~__1/messages", params=create_message("hi")
    )
    return response.content


if __name__ == "__main__":
    send_message("bah!!")
    send_message("bih!!")
    print(get_messages())
    print("got {} from bad!".format(bad_request()))
