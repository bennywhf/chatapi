from dal.data_access import DBAccessor
import re


class BadChannelIdException(Exception):
    pass


class ChatController:
    def __init__(self, settings):
        self.settings = settings

    def get_messages_starting_at_message_id(self, channel_id, message_id):
        if self.check_channel_id(channel_id):
            return {
                "messages": DBAccessor(
                    self.settings["db"]
                ).get_channel_messages_from_id(channel_id.lower(), message_id)
            }
        else:
            raise BadChannelIdException

    def add_channel_message(self, channel_id, message_as_dict):
        if self.check_channel_id(channel_id):
            return DBAccessor(self.settings["db"]).add_message(
                channel_id.lower(), message_as_dict
            )
        else:
            raise BadChannelIdException

    def check_channel_id(self, channel_id):
        return bool(re.match(r"^([a-zA-Z0-9]|-)*$", channel_id))
