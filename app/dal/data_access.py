from dal.data_store import DataFetcher


class DBAccessor:
    def __init__(self, db_config):
        self.fetcher = DataFetcher(db_config)

    def get_channel_messages_from_id(self, channel_id, message_id):
        raw_data = self.fetcher.get_channel_messages_from_id(channel_id, message_id)
        return [self.get_message_as_dict(raw_message) for raw_message in raw_data]

    def get_message_as_dict(self, raw_message):
        message, message_id, channel_id, user_id, user_name = raw_message
        return {"message_id": message_id, "message": message, "user_name": user_name}

    def add_message(self, channel_id, message_as_dict):
        return {
            "id": self.fetcher.create_channel_message(
                message_as_dict["username"], channel_id, message_as_dict["message"]
            )
        }
