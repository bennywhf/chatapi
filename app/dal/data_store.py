import psycopg2


class DataFetcher:
    def __init__(self, db_config):
        self.db_settings = db_config

    def get_channel_messages_from_id(self, channel_id, message_id):
        sql = """select m.message, m.id, m.channel_id, m.user_id, u.username 
                 from chatapi_channel_message m, chatapi_user u
                 where channel_id = %(channel_id)s
                 and m.id > %(message_id)s
                 and m.user_id = u.id"""

        return self.fetch_all_rows(sql, channel_id=channel_id, message_id=message_id)

    def create_channel_message(self, username, channel_id, message):
        user_id = self.get_or_create_user(username)[0]
        sql = """insert into chatapi_channel_message(message, channel_id, user_id)
                                             values(%(message)s, %(channel_id)s, %(user_id)s) returning *"""
        return self.insert_row(
            sql, message=message, channel_id=channel_id, user_id=user_id
        )

    def get_or_create_user(self, username):
        sql = """select * from chatapi_user where username=%(username)s"""
        user_rows = self.fetch_all_rows(sql, username=username)

        if len(user_rows) > 0:
            return user_rows[0]
        else:
            self.create_user(username)
            return self.fetch_all_rows(sql, username=username)[0]

    def create_user(self, username):
        sql = """insert into chatapi_user(username) values(%(username)s) returning id"""
        user = self.insert_row(sql, username=username)
        return user

    def fetch_all_rows(self, sql, **kwargs):
        with self.get_connection().cursor() as curs:
            curs.execute(sql, kwargs)
            return curs.fetchall()

    def insert_row(self, sql, **kwargs):
        conn = self.get_connection()
        new_row = None
        with conn.cursor() as curs:
            curs.execute(sql, kwargs)
            new_row = curs.fetchone()[0]
        conn.commit()
        return new_row

    def get_connection(self):
        return psycopg2.connect(
            host=self.db_settings["host"],
            port=self.db_settings["port"],
            user=self.db_settings["user"],
            password=self.db_settings["password"],
        )
